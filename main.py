from bottle import *
import bloginit, bloglogic
db = bloglogic.Database('blog.db')
bloginit.configure(db, 'johndoe', 'password123')
secret_key = 'EX4MPLEK3Y'

@route('/css/<filename>')
def css(filename):
    return static_file(filename, root='css')

@post('/login')
def login():
    if bloglogic.login(db,secret_key): redirect('/')
    else: return 'login failed.'

@get('/logout')
def logout():
    bloglogic.logout()
    redirect('/')

@post('/addpost')
def addpost():
    if bloglogic.loggedin(secret_key): 
        bloglogic.createpost(db)
        redirect('/')
    else: return 'You left a field blank, or are no longer logged in.'

@get('/delete/<id>')
def delpost(id):
    if bloglogic.loggedin(secret_key):
        bloglogic.deletepost(db, id)
        redirect('/')
    else: return "You can't delete a post unless you log in."
    
@get('/')
def home():
    loggedin = bloglogic.loggedin(secret_key)
    posts = bloglogic.blogposts(db)
    return template('main', tplvars={'loggedin':loggedin, 'posts':posts})

run(host='localhost', port=80, reloader=True)