from bottle import request, response
import sqlite3, hashlib, time, uuid

class Database(object):
    def __init__(self, dbname):
        self.con = sqlite3.connect(dbname)
        self.sql = self.con.cursor()

def sha(p): return hashlib.sha256(p).hexdigest()
def newsalt(): return uuid.uuid4().hex        
def logout(): response.delete_cookie("account")

def login(db,secret_key):
    '''check credentials, log user in'''
    user = request.forms.get('user')
    passw = request.forms.get('passw')
    result = db.sql.execute("SELECT * FROM users WHERE username=? LIMIT 1", (user,)).fetchone()
    if result:
        salt = result[2]
        realhash = result[1]
        attemptedhash = sha(passw+salt)
        if realhash == attemptedhash:
            user = response.set_cookie("account", user, secret=secret_key)
            return True
        else: return False
    else: return False

def loggedin(secret_key):
    '''check if user is currently logged in (session)'''
    if request.get_cookie("account", secret=secret_key): return True
    else: return False
    
def createpost(db):
    '''add new blog post to db'''
    title = request.forms.get('posttitle')
    body = request.forms.get('postbody')
    if title and body:
        db.sql.execute("INSERT INTO posts VALUES (?,?,?)", (title, body,time.time()))
        db.con.commit()

def deletepost(db,id):
    '''delete post by id'''
    result = db.sql.execute('DELETE FROM posts WHERE rowid = ?',(id,)).rowcount
    db.con.commit()
    return result
    
def blogposts(db):
    '''fetch & return list of all blog posts'''
    return db.sql.execute('SELECT title,body,time,rowid FROM posts ORDER BY time DESC').fetchall()
