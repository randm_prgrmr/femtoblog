<!DOCTYPE HTML>
<html>
<head>
    <title> femtoBlog </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="/css/blog.css" />
    <link href='http://fonts.googleapis.com/css?family=Josefin+Slab' rel='stylesheet' type='text/css' />
</head>
<body>
<div id = "maincol">
    %if not tplvars['loggedin']:
    <!-- if user not logged in, show login bar -->
        <div class="post">
            <div class="ptitle">LOGIN</div>
            <form name="newpost" action="login" method="POST">
                User: <input type="text" size="32" name="user">
                Pass: <input type="password" size="32" name="passw">  
                <input type="submit" value="Log In">
            </form>
        </div>
        
    %else:
    <!-- else show Add Blog Post form -->
        <div class="post">
            <a class="floatlink" href="/logout">LOGOUT</a>
            <div class="ptitle">ADD POST</div>
            <form name="newpost" action="addpost" method="POST">
                <input type="text" size="40" name="posttitle"><br>
                <textarea cols="50" rows="5" name="postbody" placeholder="Your Post Here"></textarea><br>
                <input type="submit" value="Post!">
            </form>
        </div>
    %end
    <!-- Display blog posts -->
    %for post in tplvars['posts']:
        <div class="post">
            <div class="ptitle">
            <a class="floatlink" href="/delete/{{post[3]}}">[delete]</a>
            {{post[0]}}    
            </div>
            <p>{{post[1]}}</p>
        </div>
    %end
</div></body></html>