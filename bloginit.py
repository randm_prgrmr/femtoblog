import bloglogic

def configure(db, username='johndoe', password='password123'):
    '''set up database and add a default user and example blog post'''
    salt = bloglogic.newsalt()
    phash = bloglogic.sha(password+salt)

    ptitle = 'Example Post'
    pbody = 'Congrats! Your installation of femtoBlog works!'

    try:
        print "starting firstrun configuration"
        db.sql.execute("CREATE TABLE users (username text, hash text, salt text)")
        db.sql.execute("CREATE TABLE posts (title text, body text, time float)")
        
        db.sql.execute("INSERT INTO users VALUES (?,?,?)",(username,phash,salt))
        db.sql.execute("INSERT INTO posts VALUES (?,?,?)",(ptitle,pbody,12345.6))
        
        db.con.commit()
        print "all configured!"
    except: "firstrun configuration -- already configured"
